package com.company;

public class Tasks {
    public void task1() {
        int randomNumber = Manager.getInstance().getRandom3();
        System.out.println("Your random number: " + randomNumber);
        System.out.println("The biggest digit of this number: " + Manager.getInstance().getBiggerDigit(randomNumber));
        Manager.getInstance().printLongLine();
    }

    public void task2() {
        int firstRandomNumber = Manager.getInstance().getRandom3();
        int secondRandomNumber = Manager.getInstance().getRandom3();
        int thirdRandomNumber = Manager.getInstance().getRandom3();

        int sum = Manager.getInstance().getFirstDigit(firstRandomNumber)
                + Manager.getInstance().getFirstDigit(secondRandomNumber)
                + Manager.getInstance().getFirstDigit(thirdRandomNumber);

        System.out.printf("Your random numbers: %d %d %d %n", firstRandomNumber, secondRandomNumber, thirdRandomNumber);
        System.out.println("The sum of their first digits: " + sum);
        Manager.getInstance().printLongLine();
    }

    public void task3() {
        int firstRandomNumber = Manager.getInstance().getRandom3();
        int secondRandomNumber = Manager.getInstance().getRandom3();
        int thirdRandomNumber = Manager.getInstance().getRandom3();

        int newNumber = Manager.getInstance().glueDigits(firstRandomNumber, secondRandomNumber);
        int result = newNumber - thirdRandomNumber;

        System.out.printf("Your random numbers: %d %d %d %n", firstRandomNumber, secondRandomNumber, thirdRandomNumber);
        System.out.println("The result: " + result);
        Manager.getInstance().printLongLine();
    }

    public void task4() {
        int sum = 0;
        int randomNumber = Manager.getInstance().getRandom3();

        int[] digits = Manager.getInstance().digitSpliter(randomNumber);
        for (int digit : digits) {
            sum += digit;
        }

        System.out.println("Your random number: " + randomNumber);
        System.out.println("The sum of it's digits: " + sum);
        Manager.getInstance().printLongLine();
    }
}

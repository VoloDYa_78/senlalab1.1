package com.company;

import java.util.Random;

public class Manager {

    private Manager() {
    }

    private static Manager instance;

    public static Manager getInstance() {
        if (instance == null) {
            instance = new Manager();
        }

        return instance;
    }


    public int getRandom3() {
        Random random = new Random();

        // random(899) + 100 is from 100 to 999
        return random.nextInt(899) + 100;
    }

    public int getBiggerDigit(int number) {
        int max = 0;
        int residual;
        for (int i = 0; i < 3; i++) {
            residual = number % 10;
            number = (number - residual) / 10;
            max = (max < residual) ? residual : max;
        }

        return max;
    }

    public int getFirstDigit(int number) {
        return number / 100;
    }

    public int glueDigits(int a, int b) {
        return a * 1000 + b;
    }

    public int[] digitSpliter(int number) {
        int[] resultNumbers = new int[3];
        resultNumbers[2] = number % 10;
        resultNumbers[1] = (number % 100 - resultNumbers[0]) / 10;
        resultNumbers[0] = (number - (resultNumbers[1] * 10 + resultNumbers[2])) / 100;

        return resultNumbers;
    }

    public void printLongLine() {
        System.out.println("--------------------------------------------------------------------------------------------------------------");
    }
}
